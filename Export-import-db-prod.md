# Export et import de la base de donnée de prod en local

En cas d'erreur de la procédure, il est possible de réinstaller la base de données en local.
```bash
yarn db:push && yarn db:seed
```

### 1 . Installer postgres sur la machine

Ubuntu
```bash
 sudo apt install postgresql
```
Mac<br>
[Download](https://www.postgresql.org/download/)

### 2 . Supprimer la le schema public LOCAL
__Attention__ de bien supprimer le schema public __LOCAL__ *(localhost horizon)*
[Screenshot](images/delete-local-db.png)


### 3 . Exporter la base de donnée PROD
__Attention__ de ne pas spammer la __PROD__ *(si l'export ne fonctionne pas, attendez un peu avant de relancer la procédure)*
1. Première étape [Screenshot](images/export-db-prod.png)
    1. Ouvrir PROD
    2. Ouvrir Database
    3. Clic droit sur la DB
    4. Outils 
    5. Backup

2. Deuxième étape [Screenshot](images/export-db-prod-1.png)
    1. Sélectionner public 
    2. Vérifier que toutes les tables sont bien sélectionnées
    3. Clic suivant

3. Troisième étape [Screenshot](images/export-db-prod-2.png)
    1. Dans settings 
    2. Format => *Plain*
    3. Aucun compression
    4. Aucun Encoding
    5. Selectionner => *Do not backup privileges (GRANT/REVOKE)* 
    6. Selectionner => *Discard objects owner*  <br>

4. Démarrage

### 4 . Ajouter l'extension citext au fichier SQL
__Attention__ la taille du fichier devrait être assez conséquente. Il est donc déconseillé de l'ouvrir tel quel. Utilisez : vim / nano <br>

A ajouter en dessous de: 'CREATE SCHEMA public;'
```sql
--
-- Name: citext; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA public;


--
-- Name: EXTENSION citext; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION citext IS 'data type for case-insensitive character strings';
```

### 5 . Importer la base de donnée prod en local

1. Première étape [Screenshot](images/import-db-prod-to-local.png)
    1. Ouvrir horizon
    2. Ouvrir Database
    3. Clic droit sur la DB
    4. Outils 
    5. Execute script

2. Sélectionner le fichier .sql généré par l'export
3. Démarrage

### 6 . Félicitations ! Vous avez réussi !








