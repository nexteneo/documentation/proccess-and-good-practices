# Conventions de nommage des APIs

## Exemple pour la ressource "Customer"

La ressource "Customer" se trouve dans le dossier `customers` au pluriel.

```
/customers
    /customers.module.ts
    /controllers
        /create-customer.controller.ts
        /delete-customer.controller.ts
        /detail-customer.controller.ts
        /edit-customer.controller.ts
        /list-customers.controller.ts
```

### Create

- Nom du fichier : `create-customer.controller.ts`
- Route : `POST /customers`
- Contrôleur : CreateCustomer
- Nom de la méthode : `create()`

### Delete

- Nom du fichier : `delete-customer.controller.ts`
- Route : `DELETE /customers/:uuid`
- Contrôleur : DeleteCustomer
- Nom de la méthode : `delete()`

### Detail

- Nom du fichier : `detail-customer.controller.ts`
- Route : `GET /customers/:uuid`
- Contrôleur : DetailCustomer
- Nom de la méthode : `detail()`

### Edit

- Nom du fichier : `edit-customer.controller.ts`
- Route : `PATCH /customers/:uuid`
- Contrôleur : EditCustomer
- Nom de la méthode : `edit()`

### List

- Nom du fichier : `list-customers.controller.ts`
- Route : `GET /customers`
- Contrôleur : ListCustomers
- Nom de la méthode : `list()`

## Routes spéciales

### Activer l'external roaming pour un client

**Exemple :** `toggle-smart-charging-config.controller.ts`
```ts
export class ToggleSmartChargingConfigCockpitController {
  @IsBoolean()
  isEnabled!: boolean
}

@Controller()
export class ToggleSmartChargingConfigCockpitController {
  @Put('/cockpit/smart-charging-configs/:uuid/is-enabled')
  async toggle() {}
}
```

### Autocomplete

- Nom du fichier : `autocomplete-customers.controller.ts`
- Route : `GET /customers-autocomplete`
- Contrôleur : AutocompleteCustomers
- Nom de la méthode : `autocomplete()`
    
