# Merges Requests

## Check-list pour une merge request parfaite

### Horizon

- [ ] **Nommage** : Les contrôleurs doivent respecter [la convention](https://gitlab.com/nexteneo/documentation/proccess-and-good-practices/-/blob/main/Apis.md?ref_type=heads#conventions-de-nommage-des-apis)
- [ ] **Autorisations** : Vérifier que les Guards sont corrects
  - Si seuls les super-admins Deck ont accès : `IsCustomerSupervisorSuperAdminGuard()`
  - Si seuls les admins Cockpit ont accès : `IsAdminGuard()`
  - etc
- [ ] **Droits** : Vérifier que les utilisateurs deck ont bien accès à **__toutes__** les ressources
  - Exemple, si on a `POST /ev-driver/1234/add-to-group {groupId: "9876"}`, il faut vérifier que l'ev-driver 1234 et le groupe 9876 sont bien liés au Customer de la requête
- [ ] **Tests E2E** :
  - Chaque contrôleur ou cron a une page de tests E2E
  - La fonction principale du contrôleur est bien testée
  - Chaque cas d'erreur est testé
  - Chaque cas de test doit être le plus court possible : on ne teste que ce qu'on veut tester
- [ ] **Erreurs & logs** :
  - Les erreurs doivent être throw avec `ErrorWithPayload()` avec du context
  - si pas d'erreur throw, alors faire un `this.logger.error()` ou `this.logger.warn()`
    - `error` en cas d'erreur de notre côté (le code source est buggé et doit être changé)
    - `warn` en cas d'erreur côté utilisateur (fonds insufisants pour lancer une charge, etc)