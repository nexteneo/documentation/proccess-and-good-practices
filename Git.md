# Git

Nos projets ont 2 branches principales protégées :

- `dev`, qui est la branche contenant les derniers devs (le sprint en cours), visible sur l'environnement staging
- `prod`, qui est le code exécuté dans le serveur de prod

## Dépendences à installer

- **Git Flow** : https://skoch.github.io/Git-Workflow/
- **Delete Merged Branches** : https://github.com/hartwork/git-delete-merged-branches

## Processus de développement d'une fonctionnalité

### Basculez sur une branche dédiée

1. Ouvrez un terminal et rendez vous au dossier du projet concerné (par exemple `cd ~/web-stack/horizon`)
2. `git switch dev` pour basculer sur la branche `dev`
3. `git pull origin dev` pour vous assurer d'avoir la dernière version de `dev`
4. `git switch -c nom-de-la-branche` pour créer une branche

Si la fonctionnalité concerne également un autre projet (par exemple la feature nécéssite un dev dans `horizon` et `deck`) alors répétez ce process pour tous les projets concernés.

### Développez

La fonctionnalité est terminée si :

- Le code est propre (pas d'erreur ESLint) et formatté (via Prettier)
- Des tests de non-régression ont été ajoutés
- Vous avez vérifié par vous même que la fonctionnalité fonctionne correctement

### Poussez vos modifications dans Gitlab

1. Lancez `git add -A`, qui permet de selectionner les fichiers qui vont être commités
2. Lancez `git commit -m "Nom du commit"`. Le nom du commit doit brievement expliquer le changement en quelques mots.
3. Vérifiez que le commit s'est bien déroulé (il ne doit pas y avoir eu d'erreur ESLint ou Prettier)
4. Lancez `git push origin feature/[nom-de-la-branche]` (par exemple, git push origin feature/add-evse-listing)
5. Cliquez sur le lien qui permet de créer une merge request en retour de la commande push
6. Dans la description de la merge request, tappez "closes " puis copiez le lien vers l'issue gitlab qui est concernée
7. Créez la merge request

### Relecture

1. Une fois la merge request créée, relisez vous une dernière fois avec l'onglet "Changes" qui affichera vos modifications.
3. Si vous êtes content des modifications apportées, ajoutez un "Reviewer" sur le côté droit de la page, qui sera chargé de relire votre travail.
4. Si tout se passe bien, il approuvera la Merge Request, que vous pourrez alors merger : le code ira dans la branche `dev`.

Si le reviewer indique des modifications à faire, créez un nouveau commit qui contiendra vos modifications :

1. Effectuez les corrections demandées
2. `git add -A` et `git commit -m "review fixes"` pour commiter les changements
4. `git push origin nom-de-la-branche` pour repousser la nouvelle version du commit. Si le push est rejetté, essayer de faire un `git pull` avant. **Ne jamais utiliser --force**.

### Retournez sur la branche `dev` en local

Une fois que votre merge request a été mergée, retournez dans votre terminal et effectuez les actions suivantes :
1. `git switch dev` pour retourner sur la branche dev
2. `git pull origin dev` pour mettre à jour votre branche dev avec les derniers changements
3. `git-delete-merged-branches --effort=3` pour supprimer la version locale de votre branche 

## Autres commandes pratiques

### Mettre en pause un développement de feature pour basculer sur une autre feature

Si jamais vous êtes en plein dev de feature, mais que vous devez interrompre momentanément le dev pour faire autre chose (développer une autre feature, ou corriger un bug, ou autre) voici la démarche :

1. `git add -A` puis `git commit -m "WIP" --no-verify` pour commiter vos changements. Le flag `--no-verify` permet de ne pas lancer les tests ni les vérifications de type. Ainsi, vous pourrez commiter un code cassé.
2. Changez de branche avec `git switch [nom-de-la-branche]` Faites ce que vous avez à faire
3. Quand vous voudrez retourner bosser sur la feature interrompue, retournez sur la branche avec `git switch [nom-de-la-branche]`
4. Faites un `git log` pour vérifier que le dernier commit est le commit `WIP`
5. Lancez `git reset HEAD~1` pour annuler ce commit `WIP` ; vous pourrez continuer là où vous en étiez.

### Régler un conlit lors d'une merge request

1. `git status` pour vérifier que vous n'avez pas de travail en cours non commité
2. `git switch dev` pour retourner sur la branche dev
3. `git pull origin dev` pour mettre à jour la branche dev par rapport à la version gitlab
4. `git switch xxx` pour retourner sur votre branche
5. `git merge dev` pour intégrer les changements de `dev`
6. Résoudre les conflits dans VSCode
7. `git add -A` quand tous les conflits sont résolus
8. `git commit -m "merge"` pour terminer
9. `git push origin xxx` pour mettre à jour la merge request

### Se mettre à jour par rapport à `dev`

⚠️ À ne faire que si vous n'avez pas de merge request en cours sur la branche

- Vérifiez que vous êtes sur votre branche avec aucun changement en cours
- `git switch dev` pour aller sur la branche dev
- `git pull origin dev` pour mettre à jour dev
- `git switch ma-branche` pour retourner sur sa branche
- `git rebase dev`
- Si y'a des conflits, les résoudre puis faire `git add -A` puis `git rebase --continue`

Pour vous sortir d'un rebase foireux : `git rebase --abort`

## Cheatsheet

### `git add` : gestion des fichiers à commiter

Permet d'ajouter des fichiers au futur commit. En effet, quand vous commitez, vous pouvez choisir de ne commiter que certains fichiers et pas d'autres.
Pour annuler un `git add`, utilisez `git reset`

Exemples :
- `git add README.md` ajoute le fichier README.md au futur commit.
- `git add -A` ajoute tous les fichiers modifiés au futur commit.
- `git reset README.md` enlève le fichier README.md du futur commit.
- `git reset` enlève tous les fichiers du futur commit.

### `git flow` : créer une branche feature, hotfix ou release

Nécéssite [git flow](https://github.com/nvie/gitflow/wiki/Installation) ([documentation](https://danielkummer.github.io/git-flow-cheatsheet/))

Exemple :
- `git flow feature start add-evses-listing` créera à partir de dev la branche `feature/add-evses-listing`
- `git flow release start v9.5.0` créera à partir de dev la branche `hotfix/v9.5.0`
- `git flow hotfix start v9.5.1` créera à partir de prod la branche `hotfix/v9.5.1`

### `git switch` : changer de branche

Exemples :
- `git switch dev` pour basculer sur la branche de dev
- `git switch feature/add-evses-listing` pour basculer sur la branche feature

### `git branch` et gestion des branches locales

`git switch` permet de lister les branches qui existent sur votre machine.

Exemples :
- `git branch` liste les branches locales
- `git branch -A` liste les branches locales et les branches distantes
- `git fetch -p` permet de synchroniser les branches locales avec les branches distantes

### Plus de commandes

Plus de commandes, voir ce fichier : https://github.com/RehanSaeed/Git-Cheat-Sheet
