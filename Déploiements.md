# Déployer sur le serveur OVH

## preprod

### Connection au serveur OVH de preprod

Changer le `USERNAME` en votre nom (première lettre prénom + nom de famille), demandez à Paulin votre mot de passe

`ssh USERNAME@staging.ws.nexteneo.com -p 64724` 

### horizon

- `sudo su - horizon` pour vous connecter en tant qu'utilisateur `horizon`
- `cd horizon` pour accéder au dossier qui contient de code
- `git branch` pour vérifier que vous êtes bien sur la branche `dev`
- `git pull origin dev` pour récupérer les dernières modifs de gitlab
- `docker-compose -f docker-compose.dev.yml -f docker-compose.redis.yml -f docker-compose.api.yml up --build -d` pour déployer
- `docker logs nexteneo-horizon-api -f` pour vérifier que l'app fonctionne correctement (ctrl+c pour quitter)
- `docker exec nexteneo-horizon-api yarn db:push` pour mettre à jour la BDD de staging
  - Si la commande échoue, **si vous savez ce que vous faites**, lancez : `docker exec nexteneo-horizon-api yarn db:push --accept-data-loss` pour mettre à jour la BDD de staging
- `exit` pour quitter

### cockpit

- `sudo su - cockpit` pour vous connecter en tant qu'utilisateur `horizon`
- `cd cockpit` pour accéder au dossier qui contient de code
- `git branch` pour vérifier que vous êtes bien sur la branche `dev`
- `git pull origin dev` pour récupérer les dernières modifs de gitlab
- `docker-compose up --build -d` pour déployer
- `exit` pour quitter

### deck

- `sudo su - deck` pour vous connecter en tant qu'utilisateur `horizon`
- `cd deck` pour accéder au dossier qui contient de code
- `git branch` pour vérifier que vous êtes bien sur la branche `dev`
- `git pull origin dev` pour récupérer les dernières modifs de gitlab
- `docker-compose up --build -d` pour déployer
- `exit` pour quitter

### ev-driver-pwa 

- `sudo su - ev-driver-pwa` pour vous connecter en tant qu'utilisateur `horizon`
- `cd ev-driver-pwa` pour accéder au dossier qui contient de code
- `git branch` pour vérifier que vous êtes bien sur la branche `dev`
- `git pull origin dev` pour récupérer les dernières modifs de gitlab
- `docker-compose up --build -d` pour déployer
- `exit` pour quitter

### ocppj-proxy

- `sudo su - ocppj-proxy` pour vous connecter en tant qu'utilisateur `horizon`
- `cd ocppj-proxy` pour accéder au dossier qui contient de code
- `git branch` pour vérifier que vous êtes bien sur la branche `dev`
- `git pull origin dev` pour récupérer les dernières modifs de gitlab
- `docker-compose up --build -d` pour déployer
- `exit` pour quitter
