# Proccess et bonnes pratiques
## Proccess
- [Procédure de déploiement](Déploiements.md)
- [Procédure d'import des données de prod en local](Export-import-db-prod.md)
## Bonne pratiques 
#### Git
- [Liste des commandes Git utiles](Git.md)
#### Apis wording
- [Bonne pratique lors du nommage dans l'API](Apis.md)